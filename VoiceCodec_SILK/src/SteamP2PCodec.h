#pragma once

#include "VoiceEncoder_Silk.h"

class CSteamP2PCodec: public IVoiceCodec {
public:
	CSteamP2PCodec(IVoiceCodec *pEncoder);
	bool Init(int quality);
	void Release();
	int Compress(const char *pUncompressedBytes, int nSamples, char *pCompressed, int maxCompressedBytes, bool bFinal);
	int Decompress(const char *pCompressed, int compressedBytes, char *pUncompressed, int maxUncompressedBytes);
	bool ResetState();

private:
	int StreamDecode(const char *pCompressed, int compressedBytes, char *pUncompressed, int maxUncompressedBytes) const;
	int StreamEncode(const char *pUncompressedBytes, int nSamples, char *pCompressed, int maxCompressedBytes, bool bFinal) const;

private:
	IVoiceCodec *m_pCodec;
};

IBaseInterface *CreateSteamP2PCodec(IVoiceCodec *pEncoder);
