#pragma once

#include "IVoiceCodec.h"
#include "utlbuffer.h"
#include "SKP_Silk_SDK_API.h"

class VoiceEncoder_Silk: public IVoiceCodec {
public:
	VoiceEncoder_Silk();
	virtual ~VoiceEncoder_Silk();

	bool Init(int quality);
	void Release();
	bool ResetState();
	int Compress(const char *pUncompressedBytes, int nSamples, char *pCompressed, int maxCompressedBytes, bool bFinal);
	int Decompress(const char *pCompressed, int compressedBytes, char *pUncompressed, int maxUncompressedBytes);

	int GetNumQueuedEncodingSamples() const { return m_bufOverflowBytes.TellPut() / BYTES_PER_SAMPLE; }

private:
	void *m_pEncoder;
	int m_API_fs_Hz;
	int m_targetRate_bps;
	int m_packetLoss_perc;
	SKP_SILK_SDK_EncControlStruct m_encControl;
	CUtlBuffer m_bufOverflowBytes;

	void *m_pDecoder;
	SKP_SILK_SDK_DecControlStruct m_decControl;
};
