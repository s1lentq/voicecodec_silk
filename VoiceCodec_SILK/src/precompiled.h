#pragma once

#include <windows.h>

#include <stdio.h>
#include <stdlib.h>

#include "interface.h"
#include "archtypes.h"
#include "crc32.h"

#include "SteamP2PCodec.h"
#include "VoiceEncoder_Silk.h"

